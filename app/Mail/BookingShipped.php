<?php

namespace App\Mail;

use App\Models\Booking;
use App\Models\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookingShipped extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var Client
     * @var Booking
     */
    public $client;
    public $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Client $client, Booking $booking)
    {
        $this->client = $client;
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New query booking')->
        view('mail.booking');
    }
}
