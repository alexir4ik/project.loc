<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;
use App\Services\BookingService;
use App\Mail\ReminderShipped;

class SendEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sendemail:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sending an appointment reminder email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(BookingService $booking)
    {
        parent::__construct();
        $this->booking = $booking;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (!empty($this->booking->infoReminder())) {
            $data = $this->booking->infoReminder();
            Mail::to($data['client']['email'])->send(new ReminderShipped($data));
        }
    }
}
