<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $table = 'feedback';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['fname', 'lname', 'phone', 'email', 'comment', 'received'];

    public $timestamps = false;

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = "+" . $value;
    }

    public function getFullNameAttribute()
    {
        return "{$this->fname} {$this->lname}";
    }
}
