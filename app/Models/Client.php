<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    use SoftDeletes;

    protected $table = 'clients';

    protected $fillable = ['fname', 'lname', 'phone', 'email', 'message'];

    public function setPhoneAttribute($value)
    {
        $this->attributes['phone'] = "+" . $value;
    }

    public function getFullNameAttribute()
    {
        return "{$this->fname} {$this->lname}";
    }

}
