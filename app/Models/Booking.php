<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Booking extends Model
{
    use SoftDeletes;

    protected $table = 'booking';

    protected $fillable = ['client_id', 'reserved_time', 'date'];

    public function getReservedTimeAttribute($value)
    {
        return $value . "-00";
    }

    public function setReservedTimeAttribute($value)
    {
        $this->attributes['reserved_time'] = explode("-", $value)[0];
    }

    public function client()
    {
        return $this->belongsTo('App\Models\Client', 'client_id', 'id');
    }

}
