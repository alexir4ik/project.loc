<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Feedback;

class FeedbackRepository extends BaseRepository
{
    public function __construct(Feedback $model)
    {
        $this->model = $model;
    }
}
