<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

interface BookingInterface
{
    public function locked($date): Collection;

    public function infoReminder(): array;

    public function lastBookings(): Collection;

    public function disabledDays(): Collection;

    public function quantityBookings($date): Model;

    public function topClients(): Collection;

}

