<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Models\Client;

class ClientRepository extends BaseRepository
{
    public function __construct(Client $model)
    {
        $this->model = $model;
    }
}
