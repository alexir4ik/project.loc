<?php


namespace App\Repositories;

use App\Repositories\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use App\Models\Booking;
use App\Repositories\Interfaces\BookingInterface;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class BookingRepository extends BaseRepository implements BookingInterface
{
    public function __construct(Booking $model)
    {
        $this->model = $model;
    }

    public function locked($date): Collection
    {
        return $this->model->where('date', $date)->get('reserved_time');
    }

    public function infoReminder(): array
    {
        $booking = $this->model->whereRaw(
            "(STR_TO_DATE(CONCAT( date, ' ', reserved_time), '%Y-%m-%d %H-%i-%s')) BETWEEN DATE_ADD(now() , INTERVAL + 55 MINUTE) AND DATE_ADD(now() , INTERVAL + 70 MINUTE)"
        )->with('client')->first();
        if (!empty($booking)) {
            return $booking->toArray();
        }
        return [];
    }

    public function lastBookings(): Collection
    {
        return $this->model->orderBy('id', 'desc')->limit(10)->get();
    }

    public function disabledDays(): Collection
    {
        return $this->model->select(DB::Raw("`date`, COUNT(*) as count"))->groupBy('date')->get();
    }

    public function quantityBookings($date): Model
    {
        return $this->model->select(DB::Raw("count('id') as count"))->whereRaw(
            "created_at BETWEEN DATE_ADD(now(), INTERVAL - ? DAY) AND now()",
            [$date]
        )->first();
    }

    public function topClients(): Collection
    {
        return $this->model->select(DB::Raw("client_id, count('client_id') as count"))->groupBy('client_id')->orderBy(
            'count',
            'desc'
        )->limit('3')->with('client')->get();
    }
}
