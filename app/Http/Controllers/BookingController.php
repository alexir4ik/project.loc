<?php

namespace App\Http\Controllers;

use App\Mail\BookingShipped;
use App\Services\ClientService;
use App\Services\BookingService;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\StoreDateBooking;
use App\Http\Requests\StoreTimeBooking;
use App\Http\Requests\StoreBooking;
use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client;

class BookingController extends Controller
{
    /**
     * @var BookingService
     */

    public function __construct(ClientService $client, BookingService $booking)
    {
        $this->client = $client;
        $this->booking = $booking;
    }

    public function selectDay()
    {
        $disabledDays = $this->booking->disabledDays();
        return view('booking.step-one', ['disabledDays' => $disabledDays]);
    }

    public function selectDayPost(StoreDateBooking $request)
    {
        $freeTime = $this->booking->freeTime($request->booking_date);
        $request->session()->put(['booking_date' => $request->booking_date]);
        $request->session()->put(['free_time' => $freeTime]);
        return redirect()->route('booking.time');
    }

    public function selectTime()
    {
        if (empty(session('booking_date'))) {
            return redirect()->route('booking.day');
        }
        return view(
            'booking.step-two',
            [
                'freeTime' => session('free_time'),
                'bookingDate' => session('booking_date')
            ]
        );
    }

    public function selectTimePost(StoreTimeBooking $request)
    {
        $request->session()->put(['booking_time' => $request->booking_time]);
        return redirect()->route('booking.info');
    }

    public function info()
    {
        if (empty(session('booking_date'))) {
            return redirect()->route('booking.day');
        } elseif (empty(session('booking_time'))) {
            return redirect()->route('booking.time');
        }
        return view(
            'booking.step-info',
            ['bookingTime' => session('booking_time'), 'bookingDate' => session('booking_date')]
        );
    }

    public function store(StoreBooking $request)
    {
        $client = $this->client->create($request->all());
        $booking = $this->booking->create(
            [
                "client_id" => $client->id,
                "reserved_time" => $request->session()->get('booking_time'),
                "date" => $request->session()->get('booking_date')
            ]
        );
        $request->session()->forget(['booking_date', 'booking_time', 'free_time']);
        Mail::to(config('variables.admin_mail'))->send(new BookingShipped($client, $booking));
        try {
            $client = new Client(config('services.twilio.sid'), config('services.twilio.token'));
        } catch (ConfigurationException $e) {
        }
        $client->messages->create(
            '+380993330828', // Text this number
            [
                'from' => '+12565888298', // From a valid Twilio number
                'body' => 'Hello Admin! New Booking!'
            ]
        );
        return redirect()->route('booking.day')->with('message', 'Your reservation is accepted. Thanks!');
    }
}
