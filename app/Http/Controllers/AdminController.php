<?php

namespace App\Http\Controllers;

use App\Services\BookingService;
use App\Services\ClientService;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function __construct(BookingService $booking, ClientService $client)
    {
        $this->booking = $booking;
        $this->client = $client;
    }

    public function index()
    {
        return view(
            'admin.index',
            [
                'bookings' => $this->booking->all(),
                'lastBookings' => $this->booking->lastBookings(),
                'quantityBookingsYear' => $this->booking->quantityBookings('365'),
                'quantityBookingsMonth' => $this->booking->quantityBookings('30'),
                'topClients' => $this->booking->topClients()
            ]
        );
    }

}
