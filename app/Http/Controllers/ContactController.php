<?php

namespace App\Http\Controllers;

use App\Mail\FeedbackShipped;
use Illuminate\Http\Request;
use App\Http\Requests\StoreFeedbackForm;
use Illuminate\Support\Facades\Mail;
use App\Services\FeedbackService;

class ContactController extends Controller
{
    public function __construct(FeedbackService $feedback)
    {
        $this->feedback = $feedback;
    }

    public function index()
    {
        return view('contact.index');
    }

    public function store(StoreFeedbackForm $request)
    {
        $feedback = $this->feedback->create(
            [
                "fname" => $request->fname,
                "lname" => $request->lname,
                "phone" => $request->phone,
                "email" => $request->email,
                "comment" => $request->comment,
                "received" => now()
            ]
        );
        Mail::to(env('MAIL_FROM_ADDRESS'))->send(new FeedbackShipped($feedback));
        if (empty(Mail::failures())) {
            $message = "Your message has been sent";
        } else {
            $message = "Your message has not been sent, please try again";
        }
        return view('contact.index', ["message" => $message]);
    }
}
