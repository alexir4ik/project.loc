<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreBooking;
use App\Http\Requests\StoreDateBooking;
use App\Http\Requests\StoreTimeBooking;
use App\Http\Requests\UpdateBooking;
use App\Services\BookingService;
use App\Services\ClientService;
use App\Models\Booking;

class AdminBookingController extends Controller
{

    public function __construct(BookingService $booking, ClientService $client)
    {
        $this->booking = $booking;
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_booking.index', ['bookings' => $this->booking->all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function selectDay()
    {
        $disabledDays = $this->booking->disabledDays();
        return view('admin_booking.create-step1', ['disabledDays' => $disabledDays]);
    }

    public function selectDayPost(StoreDateBooking $request)
    {
        $freeTime = $this->booking->freeTime($request->booking_date);
        $request->session()->put(['booking_date' => $request->booking_date]);
        $request->session()->put(['free_time' => $freeTime]);
        return redirect()->route('admin.create.time');
    }

    public function selectTime()
    {
        if (empty(session('booking_date'))) {
            return redirect()->route('admin.create.day');
        }
        return view(
            'admin_booking.create-step2',
            [
                'freeTime' => session('free_time'),
                'bookingDate' => session('booking_date')
            ]
        );
    }

    public function selectTimePost(StoreTimeBooking $request)
    {
        $request->session()->put(['booking_time' => $request->booking_time]);
        return redirect()->route('admin.create.info');
    }

    public function info()
    {
        if (empty(session('booking_date'))) {
            return redirect()->route('admin.create.day');
        } elseif (empty(session('booking_time'))) {
            return redirect()->route('admin.create.time');
        }
        return view(
            'admin_booking.create-info',
            ['bookingTime' => session('booking_time'), 'bookingDate' => session('booking_date')]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreBooking $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBooking $request)
    {
        $client = $this->client->create($request->all());
        $booking = $this->booking->create(
            [
                "client_id" => $client->id,
                "reserved_time" => $request->session()->get('booking_time'),
                "date" => $request->session()->get('booking_date')
            ]
        );
        $request->session()->forget(['booking_date', 'booking_time', 'free_time']);
        return redirect()->route("bookings.index");
    }

    /**
     * Display the specified resource.
     *
     * @param Booking $booking
     * @return \Illuminate\Http\Response
     */
    public function show(Booking $booking)
    {
        return view('admin_booking.show', ["booking" => $booking]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return
     */
    public function editDay(Booking $booking)
    {
        $disabledDays = $this->booking->disabledDays();
        return view('admin_booking.edit-step1', ['booking' => $booking, 'disabledDays' => $disabledDays]);
    }

    public function editDayPost(StoreDateBooking $request, Booking $booking)
    {
        $freeTime = $this->booking->freeTime($request->booking_date);
        $request->session()->put(['booking_date' => $request->booking_date]);
        $request->session()->put(['free_time' => $freeTime]);
        return redirect()->route('admin.edit.time', ['booking' => $booking]);
    }

    public function editTime(Booking $booking)
    {
        return view(
            'admin_booking.edit-step2',
            [
                'booking' => $booking,
                'freeTime' => session('free_time'),
                'bookingDate' => session('booking_date')
            ]
        );
    }

    public function editTimePost(StoreTimeBooking $request, Booking $booking)
    {
        $request->session()->put(['booking_time' => $request->booking_time]);
        return redirect()->route('admin.edit.info', ['booking' => $booking]);
    }

    public function editInfo(Booking $booking)
    {
        return view(
            'admin_booking.edit-info',
            [
                'booking' => $booking,
                'bookingTime' => session('booking_time'),
                'bookingDate' => session('booking_date')
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateBooking $request
     * @param int $id
     * @return //\Illuminate\Http\Response
     */
    public function update(UpdateBooking $request, Booking $booking)
    {
        $this->booking->update(
            $booking->id,
            [
                "reserved_time" => $request->session()->get('booking_time'),
                "date" => $request->session()->get('booking_date')
            ]
        );
        $this->client->update($booking->client_id, $request->all());


        return redirect()->route('bookings.show', ['booking' => $booking]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Booking $booking)
    {
        $this->client->destroy($booking->client_id);
        $this->booking->destroy($booking->id);
        return redirect()->route('bookings.index');
    }


}
