<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreBooking extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'fname' => 'required|min:2|max:50',
            'lname' => 'required|min:2|max:100',
            'phone' => 'required|regex:/(380)[0-9]{9}/',
            'email' => 'required|email',
            'message' => 'required|min:20|max:255',
        ];
    }
}
