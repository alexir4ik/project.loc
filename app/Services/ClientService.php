<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\ClientRepository;
use Illuminate\Support\Collection;

class ClientService extends BaseService
{
    public function __construct(ClientRepository $repo)
    {
        $this->repo = $repo;
    }
}
