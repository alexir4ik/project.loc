<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\BookingRepository;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;

class BookingService extends BaseService
{
    public function __construct(BookingRepository $repo)
    {
        $this->repo = $repo;
    }

    public function freeTime($date): array
    {
        $lockedTime = [];
        $freeTime = [];
        $res = $this->repo->locked($date);
        foreach ($res as $lock) {
            $lockedTime[] = $lock->reserved_time;
        }
        for ($i = 0; $i < count(config('variables.data')); $i++) {
            if (!(in_array(config('variables.data')[$i], $lockedTime))) {
                if (rtrim(config('variables.data')[$i], "-00") > date('H')) {
                    $freeTime[] = config('variables.data')[$i];
                }
            }
        }
        return $freeTime;
    }

    public function infoReminder(): array
    {
        return $this->repo->infoReminder();
    }

    public function lastBookings(): Collection
    {
        return $this->repo->lastBookings();
    }

    public function disabledDays(): array
    {
        $disabledDays = [];
        $lockedDays = $this->repo->disabledDays()->toArray();
        foreach ($lockedDays as $item) {
            if ($item['count'] >= count(config('variables.data'))) {
                $disabledDays[] = date('m/d/Y', strtotime($item['date']));
            }
        }
        return $disabledDays;
    }

    public function quantityBookings($date)
    {
        $booking = $this->repo->quantityBookings($date);
        return $booking->count;
    }

    public function topClients(): Collection
    {
        return $this->repo->topClients();
    }
}
