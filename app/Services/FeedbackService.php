<?php

namespace App\Services;

use App\Services\BaseService;
use App\Repositories\FeedbackRepository;
use Illuminate\Support\Collection;

class FeedbackService extends BaseService
{
    public function __construct(FeedbackRepository $repo)
    {
        $this->repo = $repo;
    }
}
