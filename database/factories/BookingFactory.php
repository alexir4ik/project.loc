<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use App\Models\Booking;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

    $factory->define(
        Booking::class,
        function (Faker $faker) {
            return [
                'client_id' => $faker->numberBetween(1, 10),
                'reserved_time' => $faker->numberBetween(9, 17),
                'date' => date('Y-m-d'),
            ];
        }
    );
