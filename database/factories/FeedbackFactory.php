<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Feedback;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

    $factory->define(
        Feedback::class,
        function (Faker $faker) {
            return [
                'fname' => $faker->firstName,
                'lname' => $faker->lastName,
                'phone' => $faker->ean13,
                'email' => $faker->unique()->safeEmail,
                'comment' => $faker->sentence(10),
                'received' => now(),
            ];
        }
    );
