<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIndexesToBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(
            'booking',
            function (Blueprint $table) {
                $table->index('client_id', 'client_id');
                $table->index('date', 'date');
            }
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(
            'booking',
            function (Blueprint $table) {
                $table->dropIndex('client_id');
                $table->dropIndex('date');
            }
        );
    }
}
