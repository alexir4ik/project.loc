<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>EirnnnWorld - your beauty</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/all.css') }}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js')}}"></script>
    <script src="https://use.fontawesome.com/releases/v5.0.8/js/all.js"></script>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #fff;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }
    </style>
</head>
<body>

<!--  Header -->
<header class="site-navbar py-1" role="banner">

    <div class="container-fluid">
        <div class="row align-items-center">

            <div class="col-6 col-xl-2" data-aos="fade-down">
                <h1 class="mb-0"><a href="{{ route('home') }}" class="text-black h2 mb-0">EirnnnWorld</a></h1>
            </div>
            <div class="col-10 col-md-8 d-none d-xl-block" data-aos="fade-down">
                <nav class="site-navigation position-relative text-right text-lg-center" role="navigation">

                    <ul class="site-menu js-clone-nav mx-auto d-none d-lg-block">
                        <li>
                            <a href="{{ route('home') }}">Home</a>
                        </li>
                        <li><a href="{{ route('booking.day') }}">Book Online</a></li>
                        <li><a href="{{ route('contact') }}">Contact</a></li>
                    </ul>
                </nav>
            </div>

            <div class="col-6 col-xl-2 text-right" data-aos="fade-down">
                <div class="d-none d-xl-inline-block">
                    <ul class="site-menu js-clone-nav ml-auto list-unstyled d-flex text-right mb-0" data-class="social">
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-facebook"></i></span></a>
                        </li>
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-instagram"></i></span></a>
                        </li>
                        <li>
                            <a href="#" class="pl-3 pr-3 text-black"><span><i class="fab fa-twitter"></i></span></a>
                        </li>
                    </ul>
                </div>

                <div class="d-inline-block d-xl-none ml-md-0 mr-auto py-3" style="position: relative; top: 3px;"><a
                        href="#" class="site-menu-toggle js-menu-toggle text-black"><span
                            class="icon-menu h3"></span></a></div>

            </div>

        </div>
    </div>
</header>
<!-- end Header -->

@yield('content')

<!-- Footer-->
<footer class="site-footer">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="mb-2">
                    <h3 class="footer-heading mb-2">About EirnnnWorld</h3>
                    <p class="text-white">Our salon is the best thing that can happen to you! We give each client a
                        great mood and the
                        most positive emotions!</p>
                </div>
            </div>
            <div class="col-lg-4 mb-2 mb-lg-0">
                <div class="row mb-2">
                    <div class="col-md-12">
                        <h3 class="footer-heading mb-2 text-center">Quick Menu</h3>
                    </div>
                    <div class="col-md-12 text-center">
                        <ul class="list-unstyled">
                            <li><a href="{{ route('home') }}">Home</a></li>
                            <li><a href="{{ route('booking.day') }}">Book Online</a></li>
                            <li><a href="{{ route('contact') }}">Contact Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-3 mt-3 text-center text-white">
            <div class="col-md-12">
                <div class="mb-3">
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-facebook"></i></span></a>
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-instagram"></i></span></a>
                    <a href="#" class="pl-0 pr-3"><span><i class="fab fa-twitter"></i></span></a>
                </div>
            </div>

        </div>
    </div>
</footer>
<!--end  Footer-->
<!-- Scripts -->
@yield('file-js')
@yield('js')
</body>
</html>
