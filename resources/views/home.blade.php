@extends('layouts.clients')

@section('content')

    <div class="site-blocks-cover" style="background-image: url(images/main/main_2.jpg);" data-aos="fade"
         data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">

                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h4 class="text-white font-weight-light text-uppercase">Welcome to EirnnnWorld</h4>
                    <h3 class="text-white font-weight-light mb-5 display-1">Your Beauty Salon</h3>
                    <p><a href="{{ route('booking.day') }}" class="btn btn-black py-3 px-5">Book Now!</a></p>
                </div>
            </div>
        </div>
    </div>

    <!-- section introduction -->
    <div class="site-section">
        <div class="container">
            <div class="row mt-2">
                <div class="border p-4 col-md-6 col-lg-4 text-center">
                    <h3 class="line-height-1 mb-3">
                        <span class="d-block display-4 line-height-1 text-black">Welcome to</span>
                        <span class="d-block display-5 line-height-1 text-black">Beauty Salon</span>
                        <span class="d-block display-4 line-height-1"><em
                                class="text-primary font-weight-bold">EirnnnWorld</em></span></h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Incidunt architecto ab hic rem placeat
                        eius commodi eum eligendi recusandae sed qui cumque quibusdam.</p>
                </div>
                <div class="col-md-6 col-lg-4">
                    <figure class="h-100 hover-bg-enlarge">
                        <div class="bg-image h-100 bg-image-md-height"
                             style="background-image: url('images/main/img_1.jpg');"></div>
                    </figure>
                </div>
                <div class="col-md-6 col-lg-4">
                    <div class="border p-4 d-flex align-items-center justify-content-center h-100">
                        <div class="text-center">
                            <h2 class="text-primary h2 mb-5">Opening Hours</h2>
                            <p class="mb-4">
                                <span class="d-block font-weight-bold">Mon – Sun </span>
                                09:00 AM – 18:00 PM
                            </p>
                            <p class="mb-4 pt-4">
                                <span class="d-block font-weight-bold">Come to us!</span>
                                We are always open for you
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end section introduction -->

    <!-- section Services -->
    <div class="site-section">
        <div class="container">
            <div class="row justify-content-center mb-5">
                <div class="col-md-7">
                    <h2 class="site-section-heading font-weight-light text-black text-center">Our Services</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-lg-4 text-center mb-5 mb-lg-5">
                    <div class="h-50 p-2 p-lg-5 bg-light site-block-feature-7">
                        <span class="icon flaticon-razor display-3 text-primary mb-4 d-block"></span>
                        <h3 class="text-black h4">Cosmetology office</h3>
                        <p>We will help you prevent age-related skin changes, as well as cure defects. We perform
                            cosmetic and injection procedures.</p>
                    </div>
                    <div class="h-50 bg-light site-block-feature-7 mr-auto ml-auto">
                        <img src="images/main/cosmetology.jpg" alt="" style="width: 300px; height: 225px">
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 text-center mb-5 mb-lg-5">
                    <div class="h-50 p-4 p-lg-5 bg-light site-block-feature-7">
                        <span class="icon flaticon-shave display-3 text-primary mb-4 d-block"></span>
                        <h3 class="text-black h4">Nail service</h3>
                        <p>Beautiful and well-groomed nails with perfect coverage. You can get a great manicure and
                            pedicure in just a few hours.</p>
                    </div>
                    <div class="h-50 bg-light site-block-feature-7 mr-auto ml-auto">
                        <img src="images/main/nail.jpg" alt="" style="width: 300px; height: 225px">
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 text-center mb-5 mb-lg-5">
                    <div class="h-50 p-4 p-lg-5 bg-light site-block-feature-7">
                        <span class="icon flaticon-shave display-3 text-primary mb-4 d-block"></span>
                        <h3 class="text-black h4">Hair care</h3>
                        <p>Beautiful and healthy hair with perfect color. We perform haircuts, coloring, hair and scalp
                            treatment.</p>
                    </div>
                    <div class="h-50 bg-light site-block-feature-7 mr-auto ml-auto">
                        <img src="images/main/hair.jpg" alt="" style="width: 300px; height: 225px">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end section Services -->
@endsection
