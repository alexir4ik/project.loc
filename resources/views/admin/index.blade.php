@extends('adminlte::page')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@stop

@section('content_header')
    <h1 class="m-0 text-dark">Dashboard</h1>
@stop

@section('content')

    <div class="row">
        <div class="col-md-8">
            <!-- Info Boxes Style 2 -->
            <div class="info-box mb-3 bg-info">
                <span class="info-box-icon"><i class="fas fa-stream"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Number of bookings per year</span>
                    <span class="info-box-number">{{$quantityBookingsYear}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
            <div class="info-box mb-3 bg-gradient-light">
                <span class="info-box-icon"><i class="fas fa-bars"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text">Number of bookings per month</span>
                    <span class="info-box-number">{{$quantityBookingsMonth}}</span>
                </div>
                <!-- /.info-box-content -->
            </div>

        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Top Clients</h3>

                    <div class="card-tools">
                        <span class="badge badge-success">3 Top Clients</span>
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i
                                class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove"><i
                                class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body p-0">
                    <ul class="users-list clearfix">
                        @foreach($topClients as $client)
                            <li class="mt-3 mb-3 mr-2">
                                <span class="users-list-date">{{$client['client']['lname']}}</span>
                                <span class="users-list-date">{{$client['client']['fname']}}</span>
                                <span class="users-list-date">{{$client['count'] }}</span>
                            </li>
                        @endforeach
                    </ul>
                    <!-- /.users-list -->
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>

    <!-- Latest booking-->
    <div class="card">
        <div class="card-header border-transparent">
            <h3 class="card-title"><b>Recent bookings</b></h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
            <div class="table-responsive">
                <table class="table m-0">
                    <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>FullName of client's</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($lastBookings as $booking)
                        <tr>
                            <td><a href="pages/examples/invoice.html">{{ $booking->id }}</a></td>
                            <td>{{ $booking->date }}</td>
                            <td>{{ $booking->reserved_time }}</td>
                            <td> {{$booking->client->getFullNameAttribute()}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer clearfix">
            <a href="{{route('admin.create.day')}}" class="btn btn-sm btn-info float-left">Create New Booking</a>
            <a href="{{route('bookings.index')}}" class="btn btn-sm btn-secondary float-right">View All Booking</a>
        </div>
        <!-- /.card-footer -->
    </div>
    <!-- end Latest booking -->
@stop
