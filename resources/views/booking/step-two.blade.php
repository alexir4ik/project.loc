@extends('layouts.clients')

@section('content')

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(../images/booking/booking.jpg);"
         data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h2 class="text-white font-weight-light mb-2 display-1">Book Online</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <!-- Book form -->
                    <form action="{{ route('booking.timePost' )}}" method="post" class="p-5 bg-white">
                        @csrf

                        <div class="row form-group justify-content-center">
                            <div class="col-md-6">
                                <h4>The day of the booking</h4>
                            </div>
                            <div class="col-md-6">
                                <p> {{ $bookingDate }}</p>
                            </div>
                        </div>
                        <!-- Date -->
                        <div class="row form-group justify-content-center">
                            <div class="col-md-6">
                                <h4>Choose time for booking</h4>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control px-2 @error('booking_time') is-invalid @enderror"
                                        name="booking_time" id="time">
                                    @for($i=0; $i < count($freeTime); $i++)
                                        <option value="{{ $freeTime[$i] }}">{{ $freeTime[$i] }}</option>
                                    @endfor
                                </select>
                                @error('booking_time')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <div class="pr-2">
                                <a href="{{route('booking.day')}}" class="btn btn-secondary">
                                    <i class="fas fa-arrow-left"></i> Back
                                </a>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-primary text-white">Continue</button>
                            </div>
                        </div>
                        <!-- end Date -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
