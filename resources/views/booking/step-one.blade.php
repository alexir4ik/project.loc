@extends('layouts.clients')

@section('content')

    @if (session('message'))
        <div class="alert alert-success text-center">
            {{ session('message') }}
        </div>
    @endif

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(../images/booking/booking.jpg);"
         data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h2 class="text-white font-weight-light mb-2 display-1">Book Online</h2>
                </div>
            </div>
        </div>
    </div>


    <div class="site-section bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <!-- Book form -->
                    <form action="{{ route('booking.dayPost' )}}" method="post" class="p-5 bg-white">
                    @csrf
                    <!-- Date -->
                        <div class="row mb-4 form-group justify-content-center">
                            <div class="col-md-6">
                                <h4>Choose date for booking</h4>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                                    </div>
                                    <input type="text" name="booking_date"
                                           class="form-control float-right @error('booking_date') is-invalid @enderror"
                                           id="reservation">
                                </div>
                                @error('booking_date')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <div class="row justify-content-end">
                            <button type="submit" class="btn btn-primary text-white">Choose time</button>
                        </div>
                        <!-- end Date -->
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('file-js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
@endsection

@section('js')
    <script type="text/javascript">
        var disabledArr = @json($disabledDays);
        var dateMin = new Date();
        dateMin.setDate(dateMin.getDate() + (dateMin.getHours() >= 17 ? 1 : 0));
        $(function () {
            $('input[name="booking_date"]').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                singleDatePicker: true,
                showDropdowns: true,
                minYear: '2020',
                maxYear: parseInt(moment().format('YYYY'), 10),
                isInvalidDate: function (arg) {
                    console.log(arg);

                    // Prepare the date comparision
                    var thisMonth = arg._d.getMonth() + 1;   // Months are 0 based
                    if (thisMonth < 10) {
                        thisMonth = "0" + thisMonth; // Leading 0
                    }
                    var thisDate = arg._d.getDate();
                    if (thisDate < 10) {
                        thisDate = "0" + thisDate; // Leading 0
                    }
                    var thisYear = arg._d.getYear() + 1900;   // Years are 1900 based

                    var thisCompare = thisMonth + "/" + thisDate + "/" + thisYear;
                    console.log(thisCompare);

                    if ($.inArray(thisCompare, disabledArr) != -1) {
                        console.log("      ^--------- DATE FOUND HERE");
                        return true;
                    }
                },
                minDate: dateMin,
            }, function (start, end, label) {
                var years = moment().diff(start, 'years');
            });
        });
    </script>
@endsection
