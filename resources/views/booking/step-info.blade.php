@extends('layouts.clients')

@section('content')

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(../images/booking/booking.jpg);"
         data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h2 class="text-white font-weight-light mb-2 display-1">Book Online</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-12 mb-5">
                    <!-- Book form -->
                    <form action="{{ route('booking.store' )}}" method="post" class="p-5 bg-white">
                    @csrf
                    <!--   Choice -->
                        <div class="row form-group justify-content-center mb-3">
                            <div class="col-md-6">
                                <h4>The day of the booking</h4>
                            </div>
                            <div class="col-md-6">
                                <p> {{ $bookingDate }}</p>
                            </div>
                        </div>
                        <div class="row form-group justify-content-center mb-4">
                            <div class="col-md-6">
                                <h4>The time of the booking</h4>
                            </div>
                            <div class="col-md-6">
                                <p> {{ $bookingTime }}</p>
                            </div>
                        </div>
                        <!--   Choice -->
                        <div class="row form-group">
                            <!-- First and Last name user -->
                            <div class="col-md-6 mb-3">
                                <label class="text-black" for="fname"><b>First Name</b></label>
                                <input type="text" id="fname" name="fname"
                                       class="form-control @error('fname') is-invalid @enderror"
                                       value="{{old('fname')}}" placeholder="Enter first name">
                                @error('fname')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="text-black" for="lname"><b>Last Name</b></label>
                                <input type="text" id="lname" name="lname"
                                       class="form-control @error('lname') is-invalid @enderror"
                                       value="{{old('lname')}}" placeholder="Enter last name">
                                @error('lname')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- User's phone number -->
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label class="text-black" for="phone"><b>Mobile phone</b></label>
                                <input type="tel" id="phone" name="phone"
                                       class="form-control @error('phone') is-invalid @enderror"
                                       value="{{old('phone')}}" placeholder="Enter phone in format 380993765589">
                                @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <!-- User's Email address -->
                            <div class="col-md-6">
                                <label class="text-black" for="email"><b>Email</b></label>
                                <input type="email" id="email" name="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       value="{{old('email')}}" placeholder="Enter email">
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>

                        <!-- User's comment -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="message"><b>Message</b></label>
                                <textarea name="message" id="message" cols="30" rows="7"
                                          class="form-control @error('message') is-invalid @enderror"
                                          placeholder="Write your notes or questions here...">{{old('message')}}</textarea>
                                @error('message')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- Button for submitting the form -->
                        <div class="row form-group justify-content-end">
                            <div class="col-md-2">
                                <a href="{{route('booking.time')}}" class="btn btn-secondary py-2 px-4 text-white">
                                    <i class="fas fa-arrow-left"></i> Back
                                </a>
                            </div>
                            <div class="col-md-2">
                                <input type="submit" value="Book Now!" class="btn btn-primary py-2 px-4 text-white">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
