@extends('adminlte::page')

@section('content_header')
    <h1 class="m-0 ml-5 text-dark">Feedback List</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid"
                               aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th> #</th>
                                <th> FullName Client's</th>
                                <th> Number phone</th>
                                <th> Email</th>
                                <th> Message</th>
                                <th> Date of receipt</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($feedbacks as $feedback)
                                <tr role="row" class="odd">
                                    <td>{{ $feedback->id }}</td>
                                    <td>{{ $feedback->getFullNameAttribute() }}</td>
                                    <td>{{ $feedback->phone }}</td>
                                    <td>{{ $feedback->email }}</td>
                                    <td>{{$feedback->comment}}</td>
                                    <td>{{$feedback->received}}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@stop
