@extends('layouts.clients')

@section('content')

    <div class="site-blocks-cover inner-page-cover" style="background-image: url(images/contact/contact_us.jpg);"
         data-aos="fade" data-stellar-background-ratio="0.5">
        <div class="container">
            <div class="row align-items-center justify-content-center text-center">
                <div class="col-md-8" data-aos="fade-up" data-aos-delay="400">
                    <h2 class="text-white font-weight-light mb-2 display-1">Contact Us</h2>
                </div>
            </div>
        </div>
    </div>

    <div class="site-section bg-light mt-4">
        <div class="container">
            <div class="row">
                <div class="col-md-7 mb-5">
                    @if(!empty($message))
                        <div class="alert alert-primary">
                            <ul>
                                <li>{{ $message }}</li>
                            </ul>
                        </div>
                @endif
                <!-- Feedback form -->
                    <form action="{{ route('contact.store' )}}" method="post" class="p-5 bg-white">
                        @csrf
                        <div class="row form-group">
                            <!-- First and Last name user -->
                            <div class="col-md-6 mb-3 mb-md-0">
                                <label class="text-black" for="fname">First Name</label>
                                <input type="text" id="fname" name="fname"
                                       class="form-control @error('fname') is-invalid @enderror"
                                       value="{{old('fname')}}">
                                @error('fname')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="col-md-6">
                                <label class="text-black" for="lname">Last Name</label>
                                <input type="text" id="lname" name="lname"
                                       class="form-control @error('lname') is-invalid @enderror"
                                       value="{{old('lname')}}">
                                @error('lname')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- User's phone number -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="phone">Mobile phone</label>
                                <input type="tel" id="phone" name="phone"
                                       class="form-control @error('phone') is-invalid @enderror"
                                       value="{{old('phone')}}">
                                @error('phone')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- User's Email address -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="email">Email</label>
                                <input type="email" id="email" name="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       value="{{old('email')}}">
                                @error('email')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- User's comment -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <label class="text-black" for="comment">Message</label>
                                <textarea name="comment" id="comment" cols="30" rows="7"
                                          class="form-control @error('comment') is-invalid @enderror"
                                          placeholder="Write your notes or questions here...">{{old('comment')}}</textarea>
                                @error('comment')
                                <div class="alert alert-danger">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>
                        <!-- Button for submitting the form -->
                        <div class="row form-group">
                            <div class="col-md-12">
                                <input type="submit" value="Send Message" class="btn btn-primary py-2 px-4 text-white">
                            </div>
                        </div>


                    </form>
                </div>
                <div class="col-md-5">

                    <div class="p-4 mb-3 bg-white">
                        <p class="mb-0 font-weight-bold">Address</p>
                        <p class="mb-4">Pervomajskaya street - 88/93, Kremenchuk, Ukraine</p>

                        <p class="mb-0 font-weight-bold">Phone</p>
                        <p class="mb-4"><a href="#">{{ config('variables.number_phone') }}</a></p>

                        <p class="mb-0 font-weight-bold">Email Address</p>
                        <p class="mb-0"><a href="#">beautysalon@example.com</a></p>

                    </div>

                    <div class="p-4 mb-3 bg-white">
                        <h3 class="h5 text-black mb-3">More Info</h3>
                        <p>Our salon is the best thing that can happen to you! We give each client a great mood and the
                            most positive emotions!</p>
                        <p><a href="#" class="btn btn-primary px-4 py-2 text-white">Learn More</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection
