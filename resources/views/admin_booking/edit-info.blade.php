@extends('adminlte::page')

@section('content_header')
    <div class="container">
        <h1 class="m-0 ml-5 text-dark">Edit Booking</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <form method="POST" action="{{route('bookings.update', ['booking' => $booking->id])}}">
            @csrf
            @method('PUT')
            <div class="card-body">
                <!--   Choice -->
                <div class="row form-group justify-content-center mb-3">
                    <div class="col-md-6">
                        <h4>The day of the booking</h4>
                    </div>
                    <div class="col-md-6">
                        <p> {{ $bookingDate }}</p>
                    </div>
                </div>
                <div class="row form-group justify-content-center mb-4">
                    <div class="col-md-6">
                        <h4>The time of the booking</h4>
                    </div>
                    <div class="col-md-6">
                        <p> {{ $bookingTime }}</p>
                    </div>
                </div>
                <!--   Choice -->
                <div class="form-group">
                    <label for="exampleInputFname">First Name</label>
                    <input name="fname" type="text" value="{{ old('fname') ?? $booking->client->fname }}"
                           class="form-control {{ $errors->has('fname') ? 'is-invalid' : '' }}" id="exampleInputFname"
                           placeholder="Enter first name">
                    @if($errors->has('fname'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('fname') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputLname">Last Name</label>
                    <input name="lname" type="text" value="{{ old('lname') ?? $booking->client->lname }}"
                           class="form-control {{ $errors->has('lname') ? 'is-invalid' : '' }}" id="exampleInputLname"
                           placeholder="Enter last name">
                    @if($errors->has('lname'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('lname') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputPhone">Mobile phone</label>
                    <input name="phone" type="tel" value="{{ old('phone') ?? ltrim($booking->client->phone, '+') }}"
                           class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}" id="exampleInputPhone"
                           placeholder="Enter phone in format 380993765589">
                    @if($errors->has('phone'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail">Email</label>
                    <input name="email" type="email" value="{{ old('email') ?? $booking->client->email}}"
                           class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" id="exampleInputEmail"
                           placeholder="Enter email">
                    @if($errors->has('email'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('email') }}</strong>
                        </div>
                    @endif
                </div>

                <div class="form-group">
                    <label class="text-black" for="message">Message</label>
                    <textarea name="message" id="message"
                              class="form-control @error('message') is-invalid @enderror"
                              placeholder="Write your notes or questions here...">{{old('message') ?? $booking->client->message}}</textarea>
                    @error('message')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <div class="text-right">
                    <a href="{{route('admin.edit.time', ['booking' => $booking->id] )}}" class="btn btn-secondary">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">Edit Booking</button>
                </div>
            </div>
        </form>
    </div>

@stop
