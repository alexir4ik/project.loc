@extends('adminlte::page')

@section('content_header')
    <h1 class="m-0 ml-5 text-dark">List of Booking</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <h5 class="card-title float-right">DataTable by date and time of booking</h5>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <div id="example2_wrapper" class="dataTables_wrapper dt-bootstrap4">
                <div class="row">
                    <div class="col-sm-12 col-md-6"></div>
                    <div class="col-sm-12 col-md-6"></div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table id="example2" class="table table-bordered table-hover dataTable dtr-inline" role="grid"
                               aria-describedby="example2_info">
                            <thead>
                            <tr role="row">
                                <th> #</th>
                                <th> Date</th>
                                <th> Time</th>
                                <th> FullName Client's</th>
                                <th> Number phone</th>
                                <th> Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($bookings as $booking)
                                <tr role="row" class="odd">
                                    <td>{{ $booking->id }}</td>
                                    <td>{{ $booking ->date }}</td>
                                    <td>{{ $booking->reserved_time }}</td>
                                    <td>{{ $booking->client->getFullNameAttribute() }}</td>
                                    <td>{{$booking->client->phone}}</td>
                                    <td>
                                        <div class="row justify-content-around">
                                            <a class="btn btn-primary btn-sm"
                                               href="{{route('bookings.show', ['booking' => $booking->id])}}">
                                                <i class="fas fa-folder">
                                                </i>
                                                View
                                            </a>
                                            <a class="btn btn-info btn-sm"
                                               href="{{route('admin.edit.day', ['booking' => $booking->id])}}">
                                                <i class="fas fa-pencil-alt">
                                                </i>
                                                Edit
                                            </a>
                                            <form action="{{route('bookings.destroy',  $booking->id)}}" method="POST">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger btn-sm"><i
                                                        class="fas fa-trash">
                                                    </i>Delete
                                                </button>
                                            </form>
                                        </div>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
@stop

@section('js')
    <script>
        $(function () {
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        });
    </script>
@stop
