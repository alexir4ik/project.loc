@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@stop


@section('content_header')
    <div class="container">
        <h1 class="m-0 ml-5 text-dark">Create Booking - Step 2 - Time</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <form method="POST" action="{{route('admin.create.timePost')}}">
            @csrf
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <h5>The day of the booking</h5>
                    </div>
                    <div class="col-md-6">
                        <p> {{ $bookingDate }}</p>
                    </div>
                </div>
                <!-- Date -->
                <div class="row form-group ">
                    <div class="col-md-6">
                        <h5>Choice time for booking</h5>
                    </div>
                    <div class="col-md-6">

                        <select class="form-control px-2" name="booking_time" id="time">
                            @for($i=0; $i < count($freeTime); $i++)
                                <option value="{{ $freeTime[$i] }}">{{ $freeTime[$i] }}</option>
                            @endfor
                        </select>

                    </div>
                </div>
                <!-- end Date -->
            </div>
            <!-- /.card-body -->
            <div class="card-footer" style="background-color: rgba(0,0,0,0);">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary text-white">Continue</button>
                </div>
            </div>
        </form>
    </div>
@stop
