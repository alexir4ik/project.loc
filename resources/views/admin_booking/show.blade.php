@extends('adminlte::page')

@section('content_header')
    <div class="container">
        <h1 class="m-0 ml-5 text-dark">Info about the client booking</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 align-items-stretch">
                <div class="card bg-light">
                    <div class="card-header text-muted border-bottom-0">
                    </div>
                    <div class="card-body pt-0">
                        <div class="row">
                            <div class="col-12">
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li><span class="fa-li"><i class="far fa-id-card"></i></span> <b>Booking Id:</b>
                                        {{ $booking->id }}
                                    </li>
                                    <li><span class="fa-li"><i class="fas fa-calendar-day"></i></span>
                                        <b> Date: </b> {{ $booking->date }}
                                    </li>
                                    <li><span class="fa-li"><i class="far fa-clock"></i></span>
                                        <b>Time: </b> {{ $booking->reserved_time}}
                                    </li>
                                </ul>
                                <hr>
                                <ul class="ml-4 mb-0 fa-ul text-muted">
                                    <li><span class="fa-li"><i class="fas fa-user"></i></span>
                                        <b>FullName Client's: </b> {{ $booking->client->getFullNameAttribute() }}
                                    </li>
                                    <li><span class="fa-li"><i class="fas fa-envelope"></i></span>
                                        <b>Email: </b> {{ $booking->client->email }}
                                    </li>
                                    <li><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> <b>Phone :</b>
                                        {{ $booking->client->phone }}
                                    </li>
                                </ul>
                                <hr>
                                <p class="text-muted text-md mt-2"><b>Message: </b> {{ $booking->client->message }} </p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="{{route('bookings.index')}}" class="btn btn-md btn-primary">
                                <i class="fas fa-arrow-left"></i> List
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
