@extends('adminlte::page')

@section('content_header')
    <div class="container">
        <h1 class="m-0 ml-5 text-dark">Edit Booking - Step 2</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <form method="POST" action="{{route('admin.edit.timePost', ['booking' => $booking->id])}}">
            @csrf
            <div class="card-body">

                <div class="row form-group">
                    <div class="col-md-6">
                        <h5>The old booking time:</h5>
                        <div class="col-md-6">
                            <p> {{ $booking->reserved_time }}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h5>New booking date:</h5>
                        <div class="col-md-6">
                            <p> {{ $bookingDate }}</p>
                        </div>
                    </div>
                </div>
                <!-- Date -->
                <div class="row form-group ">
                    <div class="col-md-6">
                        <label class="text-black " for="time">Time</label><br>
                        <select class="form-control px-2" name="booking_time" id="time">
                            @for($i=0; $i < count($freeTime); $i++)
                                <option value="{{$freeTime[$i] }}">{{ $freeTime[$i] }}</option>
                            @endfor
                        </select>
                    </div>
                </div>
                <!-- end Date -->

            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <div class="text-right">
                    <a href="{{route('admin.edit.day', ['booking' => $booking->id])}}" class="btn btn-secondary">
                        <i class="fas fa-arrow-left"></i> Back
                    </a>
                    <button type="submit" class="btn btn-primary">Edit Info</button>
                </div>
            </div>
        </form>
    </div>

@stop
