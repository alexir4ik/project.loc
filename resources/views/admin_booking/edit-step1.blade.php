@extends('adminlte::page')
@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css"/>
@stop


@section('content_header')
    <div class="container">
        <h1 class="m-0 ml-5 text-dark">Edit Booking - Step 1</h1>
    </div>
@stop

@section('content')
    <div class="container">
        <form method="POST" action="{{route('admin.edit.dayPost', ['booking' => $booking->id])}}">
            @csrf
            <div class="card-body">
                <!-- Date -->
                <div class="row form-group ">
                    <div class="col-md-6">
                        <h5>The old booking date:</h5>
                        <div class="col-md-6">
                            <p> {{ $booking->date }}</p>
                        </div>
                    </div>
                </div>
                <div class="row form-group ">
                    <div class="col-md-6">
                        <label>New date:</label>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="far fa-calendar-alt"></i></span>
                            </div>
                            <input type="text" name="booking_date" class="form-control float-right" id="reservation"
                                   value="{{ old('booking_date')}}">
                        </div>
                    </div>
                </div>
                <!-- end Date -->
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Edit Time Booking</button>
                </div>
            </div>
        </form>
    </div>

@stop

@section('js')
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script type="text/javascript">
        var disabledArr = @json($disabledDays);
        var dateMin = new Date();
        dateMin.setDate(dateMin.getDate() + (dateMin.getHours() >= 17 ? 1 : 0));
        $(function () {
            $('input[name="booking_date"]').daterangepicker({
                locale: {
                    format: 'YYYY-MM-DD'
                },
                singleDatePicker: true,
                showDropdowns: true,
                minYear: '2020',
                minDate: dateMin,
                maxYear: parseInt(moment().format('YYYY'), 10),
                isInvalidDate: function (arg) {
                    console.log(arg);

                    // Prepare the date comparision
                    var thisMonth = arg._d.getMonth() + 1;   // Months are 0 based
                    if (thisMonth < 10) {
                        thisMonth = "0" + thisMonth; // Leading 0
                    }
                    var thisDate = arg._d.getDate();
                    if (thisDate < 10) {
                        thisDate = "0" + thisDate; // Leading 0
                    }
                    var thisYear = arg._d.getYear() + 1900;   // Years are 1900 based

                    var thisCompare = thisMonth + "/" + thisDate + "/" + thisYear;
                    console.log(thisCompare);

                    if ($.inArray(thisCompare, disabledArr) != -1) {
                        console.log("      ^--------- DATE FOUND HERE");
                        return true;
                    }
                },
            }, function (start, end, label) {
                var years = moment().diff(start, 'years');
            });
        });
    </script>
@stop
