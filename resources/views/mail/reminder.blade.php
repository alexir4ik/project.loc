<!-- Styles -->
<style>
    html, body {
        margin: 0;
        padding: 0;
    }

    .container {
        margin: 15px;
    }

    .reminder {
        font-size: 16px;
        padding-bottom: 100px;
    }

    .info {
        font-size: 12px;
        padding-bottom: 25px;
    }

</style>

<div class="container">
    <h3> Hello {{ $data['client']['fname']}} </h3>

    <p class="reminder"> The visit to the EirnnnWorld beauty salon is one hour
        away( {{date('d/m/Y', strtotime($data['date'])) }} on {{ $data['reserved_time']}} ) </p>

    <p class="info">This email does not require a response. You can contact the salon by
        phone {{ config('variables.number_phone') }} or email {{ config('variables.admin_mail') }}</p>

</div>
