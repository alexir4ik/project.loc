<!-- Styles -->
<style>
    html, body {
        margin: 0;
        padding: 0;
    }

    .container {
        margin: 15px;
    }

    table {
        width: 1024px;
    }

    table, th, td {
        border: 1px solid darkgrey;
        border-spacing: 0;
        padding: 10px;
    }

    th {
        width: 200px;
    }
</style>

<div class="container">
    <h3> Hello admin! </h3>

    <p> You received new message from feedback form. Read info ... </p>

    <table class="table table-bordered">
        <tbody>
        <tr>
            <th style="text-align: left" scope="row">First name</th>
            <td>{{$feedback->fname}}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Last name</th>
            <td>{{$feedback->lname}}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Number phone</th>
            <td>{{$feedback->phone}}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Email</th>
            <td>{{$feedback->email}}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Message</th>
            <td>{{$feedback->comment}}</td>
        </tr>
        </tbody>
    </table>
</div>
