<!-- Styles -->
<style>
    html, body {
        margin: 0;
        padding: 0;
    }

    .container {
        margin: 15px;
    }

    table {
        width: 1024px;
    }

    table, th, td {
        border: 1px solid darkgrey;
        border-spacing: 0;
        padding: 10px;
    }

    th {
        width: 200px;
    }
</style>

<div class="container">
    <h3> Hello admin! </h3>

    <p> You received new message from booking form. Read info ... </p>

    <table class="table table-bordered">
        <tbody>
        <tr>
            <th style="text-align: left" scope="row">First name</th>
            <td>{{ $client->fname }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Last name</th>
            <td>{{ $client->lname }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Number phone</th>
            <td>{{ $client->phone }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Email</th>
            <td>{{ $client->email }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Booking date</th>
            <td>{{ $booking->date }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Reserved time</th>
            <td>{{ $booking->reserved_time }}</td>
        </tr>
        <tr>
            <th style="text-align: left" scope="row">Message</th>
            <td>{{ $client->message }}</td>
        </tr>
        </tbody>
    </table>
</div>
