<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(
    function () {
        Route::prefix('admin')->group(
            function () {
                Route::get('/', 'AdminController@index')->name('admin.home');
                Route::resource('bookings', 'AdminBookingController');
                Route::resource('feedback', 'AdminFeedbackController');

                Route::prefix('booking')->group(
                    function () {
                        Route::get('/step_one', 'AdminBookingController@selectDay')->name('admin.create.day');
                        Route::post('/step_one', 'AdminBookingController@selectDayPost')->name('admin.create.dayPost');
                        Route::get('/step_two', 'AdminBookingController@selectTime')->name('admin.create.time');
                        Route::post('/step_two', 'AdminBookingController@selectTimePost')->name(
                            'admin.create.timePost'
                        );
                        Route::get('/step_info', 'AdminBookingController@info')->name('admin.create.info');

                        Route::prefix('edit')->group(
                            function () {
                                Route::get('/{booking}/step_one', 'AdminBookingController@editDay')->name(
                                    'admin.edit.day'
                                );
                                Route::post('/{booking}/step_one', 'AdminBookingController@editDayPost')->name(
                                    'admin.edit.dayPost'
                                );
                                Route::get('/{booking}/step_two', 'AdminBookingController@editTime')->name(
                                    'admin.edit.time'
                                );
                                Route::post('/{booking}/step_two', 'AdminBookingController@editTimePost')->name(
                                    'admin.edit.timePost'
                                );
                                Route::get('/{booking}/step_info', 'AdminBookingController@editInfo')->name(
                                    'admin.edit.info'
                                );
                            }
                        );
                    }
                );
            }
        );
    }
);

Route::get('/', 'HomeController@index')->name('home');
Route::get('/contact_us', 'ContactController@index')->name('contact');
Route::post('/contact_us', 'ContactController@store')->name('contact.store');

Route::prefix('booking')->group(
    function () {
        Route::get('/step_one', 'BookingController@selectDay')->name('booking.day');
        Route::post('/step_one', 'BookingController@selectDayPost')->name('booking.dayPost');
        Route::get('/step_two', 'BookingController@selectTime')->name('booking.time');
        Route::post('/step_two', 'BookingController@selectTimePost')->name('booking.timePost');
        Route::get('/step_info', 'BookingController@info')->name('booking.info');
        Route::post('/booking', 'BookingController@store')->name('booking.store');
    }
);

